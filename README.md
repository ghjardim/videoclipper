# videoclipper

Produces video clips from a list

# Usage

First, with the video on your local machine, produce a text file, for example `clipper.txt`, with the following formatting: per line, add `Initial timestamp | Final timestamp | Clip name`

For example:
```
00:30:56 | 00:32:02 | Take care with your environment
00:32:02 | 00:34:00 | How to install a script
01:40:02 | 01:42:59 | Results
```

Then execute the script: `videoclipper -v Video.mp4 -l clipper.txt`

# Output

The script will create one video per line of the list. In our example, the following videos would be produced: `Take care with your environment.mp4`, `How to install a script.mp4`, `Results.mp4`.

# Copyright

Copyright (C) 2022 Guilherme H. Jardim ghjardim@mail.shiori.com.br. \
License GPLv3+: GNU GPL version 3 or later https://gnu.org/licenses/gpl.html.
